#!/usr/bin/Rscript

require(rjson)
require(crayon)
#require(plot3D)

today <- strftime(Sys.time(), format = "%Y-%m-%d")
flat <- "Flat Galaxy Society"
eddbdir <- "/home/poinck/gits/ed/eddb/"

systems_list <- fromJSON(file = paste0(eddbdir,
    today, "systems_populated.json"))

flats <- list()
i_flat <- 1
for (i in 1:length(systems_list)) {
    #cat("'", systems_list[[i]]$controlling_minor_faction, "'\n", sep = "")
    if (length(systems_list[[i]]$controlling_minor_faction) > 0) {
        if (systems_list[[i]]$controlling_minor_faction == flat) {
            flats[[i_flat]] <- systems_list[[i]]
            i_flat <- i_flat + 1
            cat(".")
        }
    }
    #cat(bold(red("??\n")))
}
cat("", length(flats), "\n")

svg(filename = paste0(eddbdir, today, "flats.svg"),
    width = 10, height = 11, pointsize = 16, family = "Monoid")
par(mar = c(3, 3, 2, 1))

x_range <- range(c(-50, 100))
y_range <- range(c(-50, 100))
flat_id <- flats[[1]]$controlling_minor_faction_id
cat("flat is", bold(flat_id), "\n")
plot(flats[[1]], xlim = x_range, ylim = y_range,
    main = "FLAT SPACE", lwd = 3, col = "darkorange")
for (f in 2:length(flats)) {
    par(new = TRUE)
    plot(flats[[f]], xlim = x_range, ylim = y_range,
        axes = FALSE, xlab = "", ylab = "",
        lwd = 3, col = "darkorange")
}

dev.off()

#svg(filename = paste0("/home/steam/eddb/", today, "flat3d.svg"),
#    width = 10, height = 11, pointsize = 16, family = "Monoid")
#png(filename = paste0("/home/steam/eddb/", today, "flat3d.png"),
#    family = "Monoid",
#    type = "cairo",
#    width = 1168, height = 1117,
#    pointsize = 16)
#par(mar = c(3, 3, 2, 1))
#z_range <- range(c(-200, -50))
#flat_id <- flats[[1]]$controlling_minor_faction_id
#cat("flat is", bold(flat_id), "\n")
#points3D(flats[[1]]$x, flats[[1]]$y, flats[[1]]$z,
#    phi = 20, theta = 40,
#    col = "#0000ff78", lwd = 6,#lighting = FALSE,
#    xlim = x_range, ylim = y_range, zlim = z_range,
#    main = "FLAT SPACE"
#)
#for (f in 2:length(flats)) {
    #lines3D(flats[[f]]$x, flats[[f]]$y, flats[[f]]$z,
    #    type = "h",
    #    add = TRUE,
    #    xlim = x_range, ylim = y_range, zlim = z_range,
    #    col = "gray", lwd = 1, lty = 4, alpha = 0.15 
    #)
#    size <- 10
#    box3D(flats[[f]]$x - size, flats[[f]]$y - size, flats[[f]]$z - size,
#        flats[[f]]$x + size, flats[[f]]$y + size, flats[[f]]$z + size,
#        add = TRUE,
#        xlim = x_range, ylim = y_range, zlim = z_range,
#        col = "#0000ff07", lwd = 2#, alpha = 0.15 
#    )
    #points3D(flats[[f]]$x, flats[[f]]$y, flats[[f]]$z,
    #    add = TRUE,
    #    xlim = x_range, ylim = y_range, zlim = z_range,
    #    col = "#0000ff", lwd = 6, alpha = 0.15 
    #)
#    plot3d(flats[[f]], xlim = x_range, ylim = y_range, zlim = z_range,
#        add = TRUE, type = "p", size = 10,
#        lwd = 6, col = "#ff000078")
#}
    # XXX no rgl for now
    # XXX and not other static 3D

#dev.off()
