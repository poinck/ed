#!/usr/bin/Rscript

ranking <- read.csv(
    paste0("./squadron_ranking.csv"),
)

#bg <- "#dddddd"
bg <- "#cccccc"

png(filename = paste0("./squadron_ranking.png"),
    #family = "RiftSoft-Bold",
    family = "56th Street",
    bg = bg, #col = "#",
    type = "cairo",
    #width = 1200, height = 800,
    width = 768, height = 1080,
    pointsize = 20
)
par(mar = c(2.5, 4.5, 2, 0.5))

yrange = range(c(1, 1000
    #max(ranking$exploration, na.rm = TRUE),
    #max(ranking$combat, na.rm = TRUE),
    #max(ranking$trade, na.rm = TRUE),
    #max(ranking$cqc, na.rm = TRUE),
    #max(ranking$xeno, na.rm = TRUE)
))

ranking$mean <- (ranking$exploration + ranking$trade + ranking$combat + ranking$xeno) / 4
print(ranking)

xrange <- as.POSIXct(range(c(
    max(ranking$date, na.rm = TRUE),
    "3308-11-10"
    #as.POSIXct("3308-09-29", format = "%Y-%m-%d")
)))

plot(as.POSIXct(ranking$date),
    ranking$exploration[ranking$season_end == 1],
    axes = FALSE,
    xlab = "", ylab = "",
    col = rgb(0.262, 0.803, 0.502, .5),
    type = "p", pch = 1, lwd = 6,
    log = "y", ylim = yrange, xlim = xrange
)

entry_last <- length(ranking$mean)
entry_10th_last <- entry_last - 12
lm_m <- lm(ranking$mean[entry_10th_last:entry_last] ~ as.POSIXct(ranking$date[entry_10th_last:entry_last]))
#cat("??\n")
par(new = TRUE)
plot(as.POSIXct(ranking$date[as.integer(unlist(attributes(predict(lm_m)))) + entry_10th_last - 1]),
    predict(lm_m),
    axes = FALSE,
    xlab = "", ylab = "",
    col = "#ffffffcc",
    type = "l",
    lwd = 2, lty = 3,
    log = "y", ylim = yrange, xlim = xrange
)

# exploration
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$exploration,
    panel.first = {
        abline(h = c(10, 5, 1), lty = 3, col = "#aaaaaa", lwd = 2)
        #abline(coef = coefficients(lm_m), col = "#767676cc", lwd = 2, lty = 3)
    },
    main = "E N E X   S Q U A D R O N   R A N K I N G S",
    xlab = "Date", ylab = "Rank in logarithmic scale",
    col = "seagreen3",
    type = "S", lwd = 4,
    log = "y", ylim = yrange, xlim = xrange
)

# combat
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$combat,
    axes = FALSE,
    xlab = "", ylab = "",
    col = paste0(bg, "cc"), type = "S", lwd = 11,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date),
    ranking$combat[ranking$season_end == 1],
    axes = FALSE,
    xlab = "", ylab = "",
    col = rgb(0.424, 0.651, 0.804, .5),
    type = "p", pch = 1, lwd = 6,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$combat,
    axes = FALSE,
    xlab = "", ylab = "",
    col = "skyblue3", type = "S", lwd = 4,
    log = "y", ylim = yrange, xlim = xrange
)

# trade
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$trade,
    axes = FALSE,
    xlab = "", ylab = "",
    col = paste0(bg, "cc"), type = "S", lwd = 11,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date),
    ranking$trade[ranking$season_end == 1],
    axes = FALSE,
    xlab = "", ylab = "",
    col = rgb(0.804, 0.376, 0.565, .5),
    type = "p", pch = 1, lwd = 6,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$trade,
    axes = FALSE,
    xlab = "", ylab = "",
    col = "hotpink3", type = "S", lwd = 4,
    log = "y", ylim = yrange, xlim = xrange
)

# cqc
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$cqc,
    axes = FALSE,
    xlab = "", ylab = "",
    col = paste0(bg, "cc"), type = "S", lwd = 11,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date),
    ranking$cqc[ranking$season_end == 1],
    axes = FALSE,
    xlab = "", ylab = "",
    col = rgb(0.933, 0.933, 0, .5),
    type = "p", pch = 1, lwd = 6,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$cqc,
    axes = FALSE,
    xlab = "", ylab = "",
    col = "yellow2", type = "S", lwd = 4,
    log = "y", ylim = yrange, xlim = xrange
)

# xeno
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$xeno,
    axes = FALSE,
    xlab = "", ylab = "",
    col = paste0(bg, "cc"), type = "S", lwd = 11,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date),
    ranking$xeno[ranking$season_end == 1],
    axes = FALSE,
    xlab = "", ylab = "",
    col = rgb(0.333, 0.102, 0.545, .5),
    type = "p", pch = 1, lwd = 6,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$xeno,
    axes = FALSE,
    xlab = "", ylab = "",
    col = "purple4", type = "S", lwd = 4,
    log = "y", ylim = yrange, xlim = xrange
)

# mean
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$mean,
    axes = FALSE,
    xlab = "", ylab = "",
    col = "#ffffff34", type = "l", lwd = 18,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$mean,
    axes = FALSE,
    xlab = "", ylab = "",
    col = "#ffffffaa", type = "p", lwd = 6, pch = 1,
    log = "y", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(as.POSIXct(ranking$date), ranking$mean,
    axes = FALSE,
    xlab = "", ylab = "",
    col = "white", type = "l", lwd = 4, lty = 3,
    log = "y", ylim = yrange, xlim = xrange
)

l <- length(ranking$date)
text(as.POSIXct(ranking$date[l]) + 280000, ranking$exploration[l] * 1.15,
    labels = ranking$exploration[l],
    font = 2, col = "seagreen3", pos = 2
)
text(as.POSIXct(ranking$date[l]) + 280000, ranking$combat[l] * 1.15,
    labels = ranking$combat[l],
    font = 2, col = "skyblue3", pos = 2
)
text(as.POSIXct(ranking$date[l]) + 280000, ranking$trade[l] * 0.85,
    labels = ranking$trade[l],
    font = 2, col = "hotpink3", pos = 2
)
text(as.POSIXct(ranking$date[l]) + 280000, ranking$xeno[l] * 1.15,
    labels = ranking$xeno[l],
    font = 2, col = "purple4", pos = 2
)
text(as.POSIXct(ranking$date[l]) + 280000, ranking$cqc[l] * 1.15,
    labels = ranking$cqc[l],
    font = 2, col = "yellow2", pos = 2
)
text(as.POSIXct(ranking$date[l]) + 280000, ranking$mean[l] * 0.8,
    labels = ranking$mean[l],
    font = 2, col = "white", pos = 2
)

season_ranking <- subset(ranking, ranking$season_end == 1, drop = TRUE)
best_trader <- min(season_ranking$trade, na.rm = TRUE)
best_combatr <- min(season_ranking$combat, na.rm = TRUE)
best_explorationr <- min(season_ranking$exploration, na.rm = TRUE)
best_xenor <- min(season_ranking$xeno, na.rm = TRUE)
before <- length(season_ranking$trade) - 1
before_trader <- season_ranking$trade[[before]]
before_combatr <- season_ranking$combat[[before]]
before_explorationr <- season_ranking$exploration[[before]]
before_xenor <- season_ranking$xeno[[before]]
last <- length(season_ranking$trade)
current_tradeb <- ""
if (best_trader == season_ranking$trade[[last]]) {
    current_tradeb <- " ++"
}
current_combatb <- ""
if (best_combatr == season_ranking$combat[[last]]) {
    current_combatb <- " ++"
}
current_explorationb <- ""
if (best_explorationr == season_ranking$exploration[[last]]) {
    current_explorationb <- " ++"
}
current_xenob <- ""
if (best_xenor == season_ranking$xeno[[last]]) {
    current_xenob <- " ++"
}

legend("bottomleft", legend = c(
        paste0("EXPLORATION = ", before_explorationr, "/", best_explorationr,
            current_explorationb),
        paste0("TRADE = ", before_trader, "/", best_trader, current_tradeb),
        paste0("COMBAT = ", before_combatr, "/", best_combatr,
            current_combatb),
        paste0("XENO DEFENCE = ", before_xenor, "/", best_xenor,
            current_xenob),
        "CQC",
        "MEAN and PREDICTED MEAN"
    ),
    col = c("seagreen3", "hotpink3", "skyblue3",
        "purple4",
        "yellow2",
        "white"
    ),
    lwd = 4,
    lty = c(1, 1, 1, 1, 1, 3)
)
legend("topleft", legend = c(
        "rank at season end in respective colour",
        "weekly mean rank"
    ),
    pch = 1, lwd = 6, lty = NA,
    col = c("black", "white")
)

dev.off()