/*
 * display, by poinck, CC0
 */

#include <LiquidCrystal.h>

String s1 = "";
String s2 = "";

const int pin_RS = 8; 
const int pin_EN = 9; 
const int pin_d4 = 4; 
const int pin_d5 = 5; 
const int pin_d6 = 6; 
const int pin_d7 = 7; 
const int pin_BL = 10; 
LiquidCrystal lcd(pin_RS,  pin_EN,  pin_d4,  pin_d5,  pin_d6,  pin_d7);

void setup() {
  Serial.begin(9600);
  //Serial.println("..");
    
  lcd.begin(16, 2);
  analogWrite(pin_BL, 32);
  lcd.print("_");
}


void loop() {
  if (Serial.available() > 0) {
    // read the incoming string:
    String incoming = Serial.readString();

    // debug: prints the received data
    //Serial.print("echo: ");
    //Serial.println(incoming[2]);

    int row = 0;
    if (incoming.startsWith("1")) {
      row = 0;
    }
    else if (incoming.startsWith("2")) {
      row = 1;
    }

    // write new line
    lcd.setCursor(0, row);
    lcd.print("                ");
    delay(123);
    int i = 1;
    while (i < 17) {
      lcd.setCursor(i-1, row);
      lcd.print(incoming[i]);
      delay(23);
      i = i + 1;
    }

    // blink on second row
    if (row == 1) {
      if (incoming.indexOf("+") > 0) {
        analogWrite(pin_BL, 64);
        delay(20);
        analogWrite(pin_BL, 96);
        delay(30);
        analogWrite(pin_BL, 128);
        delay(40);
        analogWrite(pin_BL, 160);
        delay(50);
        analogWrite(pin_BL, 192);
        delay(60);
        analogWrite(pin_BL, 224);
        delay(70);
        analogWrite(pin_BL, 192);
        delay(60);
        analogWrite(pin_BL, 160);
        delay(50);
        analogWrite(pin_BL, 128);
        delay(40);
        analogWrite(pin_BL, 96);
        delay(30);
        analogWrite(pin_BL, 64);
        delay(20);
        analogWrite(pin_BL, 32);
      }
    }
  }
}
