#!/usr/bin/Rscript

require(crayon)
require(plotrix)
require(jpeg)

today <- strftime(Sys.time(), format = "%Y-%m-%d")

exploration <- read.csv(
    paste0("/home/poinck/gits/ed/jumps/exploration.csv"),
)
#exploration$datetime <- as.POSIXct(exploration$datetime)
#str(exploration)
bodies <- read.csv(
    paste0("/home/poinck/gits/ed/jumps/bodies.csv"),
)
str(bodies)
cat(bold("explored_bodies:\n"))
explored_bodies <- merge(exploration, bodies, by = "address",
    all.x = FALSE, all.y = FALSE, sort = FALSE)
str(explored_bodies)

png(filename = paste0("./jumps/", today, "exploration.png"),
    family = "Monoid",
    bg = "#121212", col = "#eeeeee",
    type = "cairo",
    width = 1168, height = 1108,
    pointsize = 14)
par(mar = c(0, 0, 0, 0)
    #xaxs = "i",
    #yaxs = "i"
    #col = "white", col.axis = "white", col.lab = "white", col.main = "white",
    #col.sub = "white"
)

crp <- colorRampPalette(c("blue", "grey", "red"), interpolate = "linear")
minaz <- 3000
mz <- 6000
crpa <- crp(mz)
get_zcolors <- function(z) {
    c <- vector()

    for (zi in z) {
        zia <- zi + minaz
        #cat(zi, zi / mz, zia, "\n")
        c <- c(c, paste0(crpa[as.integer(zia)], "23"))
    }
    #print(c)

    return(c)
}

get_zcolor <- function(z) {
    zia <- z + minaz
    c <- paste0(crpa[as.integer(zia)], "ff")

    return(c)
}

xy_range <- range(c(
    max(exploration$x, na.rm = TRUE),
    min(exploration$x, na.rm = TRUE),
    max(exploration$y, na.rm = TRUE),
    min(exploration$y, na.rm = TRUE)
))

# my current position
cx <- exploration$x[length(exploration$x)]
cz <- signif(exploration$z[length(exploration$z)], digits = 4)
cy <- exploration$y[length(exploration$y)]
cat(green(bold("coordinates: "), "x =", cx, " y =", cy, " z =", cz, "\n"))

# EE position
# - position where I joined Endurance Exploration 
hx <- 8747.1875
hz <- -42.75
hy <- 42002.40625
dhx <- abs(hx - cx)
dhz <- abs(hz - cz)
dhy <- abs(hy - cy)
dh <- signif(sqrt(dhx**2 + dhy**2 + dhz**2), digits = 5)

radius <- 7500
x_range <- range(c(cx - radius, cx + radius))
y_range <- range(c(cy - radius, cy + radius))
plot(explored_bodies$x, explored_bodies$y,
    panel.first = {
        abline(h = 0, lty = 3, col = "#676767", lwd = .75)
        abline(v = 0, lty = 3, col = "#676767", lwd = .75)
        draw.circle(0, 0,
            seq(5000, 65000, 5000),
            lwd = .75, border = "#676767", lty = 3)
        draw.circle(0, 0,
            seq(1000, 65000, 1000),
            lwd = .5, border = "#676767", lty = 3)
        lines(c(0, cx), c(0, cy), col = "darkorange", lty = 3, lwd = .75)
        #lines(c(hx, cx), c(hy, cy), col = "green4", lty = 3, lwd = 1.5)
    },
    ylab = "y", xlab = "x", col = get_zcolors(explored_bodies$z),
    #pch = 5,   # rauten, unfilled
    #pch = 19,  # big circles, filled
    #pch = 20,  # small circles, filled
    #pch = 22,  # squares, unfilled
    #pch = 23,   # smaller rauten, unfilles
    pch = 3,
    lwd = 3,
    ylim = y_range, xlim = x_range,
    cex = explored_bodies$count * .05
)
text(c(0, 0, 0, 400, 400, 400,
        5200, 10200, 15400, 20400, 25400, 30400),
    c(-5250, -10550, -15750, 53050, 58300, 63600,
        0, 0, 0, 0, 0, 0),
    col = "#676767", font = 3,
    labels = c("5 kLy", "10 kLy", "15 kLy", "50 kLy", "55 kLy", "60 kLy",
        "5 kLy", "10 kLy", "15 kLy", "20 kLy", "25 kLy", "30 kLy"),
    cex = .7, pos = 1,
)

text(cx - radius, cy - radius,
    col = "darkorange", font = 2,
    labels = "JETT RENO",
    cex = 1, pos = 4,
)
text(cx - radius, cy - radius + 800,
    col = get_zcolor(cz
        #min(exploration$z, na.rm = TRUE),
        #max(exploration$z, na.rm = TRUE)
    ), font = 1,
    labels = paste0("z = ", cz, " Ly"),
    cex = .7, pos = 4,
)

# distance from SOL
# - line is drawn in above plot() at panel.first
d <- signif(sqrt(cx**2 + cy**2 + cz**2), digits = 5)
text(cx - radius, cy - radius + 300,
    col = "darkorange", font = 1,
    labels = paste0("distance to SOL = ", d, " Ly"),
    cex = .7, pos = 4,
)

points(exploration$x[length(exploration$x)],
    exploration$y[length(exploration$y)], col = "#eeeeee", pch = 1, lwd = 3
)
text(exploration$x[length(exploration$x)],
    exploration$y[length(exploration$y)],
    col = "#eeeeee",
    #col = "darkorange",
    #font = 2,
    labels = gsub("_", " ", exploration$system[length(exploration$system)]),
    cex = .7, pos = 3,
)

# distance to beagle point
bx <- abs(-1111.5625 - cx)
bz <- abs(-134.21875 - cz)
by <- abs(65269.75 - cy)
d <- signif(sqrt(bx**2 + by**2 + bz**2), digits = 5)
text(cx - radius, cy - radius + 550,
    col = "darkorange", font = 1,
    labels = paste0("distance to BEAGLE POINT = ", d, " Ly"),
    cex = .7, pos = 4,
)

labels <- data.frame(
    x = c(
        #unique(exploration$x[exploration$system == "Sagittarius A*"]),
        0,
        unique(exploration$x[exploration$system == "Colonia"]),
        -1111.5625,
        hx
        #-41.3125 
    ),
    y = c(
        #unique(exploration$y[exploration$system == "Sagittarius A*"]),
        0,
        unique(exploration$y[exploration$system == "Colonia"]),
        65269.75,
        hy
        #-58.96875
    ),
    label = c(
        #"A*",
        "Sol",
        "Colonia",
        "Beagle Point",
        "EE"
        #"HIP 22460"
    )
)
for (i in 1:length(labels$label)) {
    points(labels$x[i], labels$y[i],
        col = c(rep("#bbbbbb", 4), "yellow"),
        #col = "green4",
        pch = c(rep(2, 4), 3), lwd = 3, cex = .9
    )
    #polygon(c(labels$x[i] - 2000, labels$x[i] + 2000,
    #        labels$x[i] + 2000, labels$x[i] - 2000),
    #    c(labels$y[i] + 500, labels$y[i] + 500,
    #        labels$y[i] + 1500, labels$y[i] + 1500),
    #    border = NA, col = "#ffffffaa")
        # temporarly disable white background for labels (tuned to default
        # extent)
    text(labels$x[i], labels$y[i] -15,
        #col = "#bbbbbb",
        col = "green4",
        font = 2,
        labels = labels$label[i],
        cex = .7, pos = 1,
    )
    if (labels$label[i] == "EE") {
        text(labels$x[i] + 15, labels$y[i] - 15,
            col = "green4",
            font = 1,
            labels = paste0("distance = ", dh, " Ly"),
            cex = .7, pos = 4,
        )
    }
}

text(cx + radius, cy - radius,
    col = "darkorange", font = 2,
    labels = "WAYFARER EE1-E",
    cex = 1, pos = 2,
)
text(cx + radius, cy - radius + 300,
    col = "darkorange", font = 1,
    #labels = "3307-08-30..",
    labels = today,
    cex = .7, pos = 2,
)
rows <- 10
cyb <- rep(seq(from = 500, by = 1500, length.out = rows), 2)
cyt <- rep(seq(from = 1900, by = 1500, length.out = rows), 2)
str(cyb)
str(cyt)
path <- "/home/poinck/dateien/bilder/ed/"
images <- c(
    #paste0(path, "2021-08-27monkeyhead_s.jpg"),
    #paste0(path, "2021-08-30b_s.jpg"),
    #paste0(path, "2021-09-06niceparking_s.jpg"),
    #paste0(path, "2021-09-06farammonia_s.jpg"),
    #paste0(path, "2021-09-07tubus_s.jpg"),
    #paste0(path, "2021-09-07tussock_s.jpg"),
    #paste0(path, "2021-09-08nothingfound2_s.jpg"),
    #paste0(path, "2021-09-09tubus_s.jpg"),
    #paste0(path, "2021-09-09mountain_s.jpg"),
    #paste0(path, "2021-09-09bettertubes2_s.jpg"),
    #paste0(path, "2021-09-13acrosstheuniverse_s.jpg")
    #paste0(path, "2021-09-13fumerola_s.jpg"),
    #paste0(path, "2021-09-13caputus2_s.jpg"),
    #paste0(path, "2021-09-13flabellum_s.jpg"),
    #paste0(path, "2021-09-14funguida_s.jpg")
    #paste0(path, "2021-09-15ignis_s.jpg")
    #paste0(path, "2021-09-22recepta2_s.jpg")
    #paste0(path, "2021-10-08longway_s.jpg")
    #paste0(path, "2021-11-18hr2926_s.jpg"),
    #paste0(path, "2021-12-17cobra_s.jpg"),
    #paste0(path, "2022-02-03cobra_s.jpg"),
    #paste0(path, "2022-02-26argon_s.jpg")
    #paste0(path, "2022-03-22small_s.jpg")
    paste0(path, "2022-07-23cobra_s.jpg")
)
#str(images)

for (i in 1:length(images)) {
    image <- readJPEG(images[i])
    image <- abind::abind(image, image[, , 1])
    #str(image)
    image[, , 4] <- .75
    loff <- 2250
    roff <- 100
    if (i > rows) {
        loff <- 4400
        roff <- 2350
    }
    rasterImage(image, cx + radius - loff, cy - radius + cyb[i],
        cx + radius - roff, cy - radius + cyt[i], interpolate = FALSE)
    rect(cx + radius - loff, cy - radius + cyb[i],
        cx + radius - roff, cy - radius + cyt[i],
        border = "#eeeeee", lwd = .7, lty = 3)
}

start_end_seq <- seq(
    from = round(as.POSIXct(exploration$datetime[1],
        format = "%Y-%m-%dT%H:%M:%SZ",
        #format = "%Y-%m-%d",
        tz = "UTC"), units = "days") - 345600,
    to = as.POSIXct(exploration$datetime[length(exploration$datetime)],
        format = "%Y-%m-%dT%H:%M:%SZ", tz = "UTC"),
    by = "1 day")

jett_plot <- function(
    csv_file,
    offset, factor,
    column, fun,
    label,
    dt_seq,
    col, lwd
) {
    csv <- read.csv(csv_file)
    str(csv)
    time <- as.POSIXct(csv$datetime, format = "%Y-%m-%dT%H:%M:%SZ",
        tz = "UTC")
    time_aggr <- cut(x = time, breaks = dt_seq)
    aggr <- aggregate(csv[[column]], FUN = fun,
        by = list(time_aggr = time_aggr), na.rm = TRUE)
    aggr$time_aggr <- as.POSIXct(aggr$time_aggr, tz = "UTC")

    # fill in blank dates
    for (d in dt_seq) {
        if (! as.POSIXct(d, origin = "1970-1-1") %in% unlist(aggr$time_aggr)) {
            d <- round(as.POSIXct(d, origin = "1970-1-1", tz = "UTC"),
                units = "days")
            aggr[nrow(aggr) + 1, ] <- c(NA, 0)
            aggr$time_aggr[nrow(aggr)] <- d
        }
    }
    aggr <- aggr[order(aggr$time_aggr), ]
    str(aggr)

    l <- length(aggr$x)
    l200 <- l - 199
    lines(x = seq(from = cx - radius + 150, by = 15,
            length.out = 200),
        y = cy - radius + aggr$x[l200:l] * factor + offset,
        col = col,
        lwd = lwd, type = "S")
    cat(l200, "-", l, "\n")
    #print(aggr$x[l200:l])
    am <- mean(aggr$x, na.rm = TRUE)
    lines(x = c(cx - radius + 150, cx - radius + 150 + 15 * 200),
        y = rep(cy - radius + am * factor + offset, 2),
        col = col, lwd = lwd / 2, lty = 3)
    text(cx - radius + 150 + 15 * 200,
        cy - radius + offset + am * factor,
        col = col, font = 1,
        labels = paste0(round(am, digits = 1), " ", label),
        cex = .7, pos = 4,
    )

    return(csv)
}

# mean jump range (per day)
jumps <- jett_plot(
    csv_file = "/home/poinck/gits/ed/jumps/exploration.csv",
    offset = 1000, factor = .1,
    column = "jump", "sum",
    label = "Ly / day",
    dt_seq = start_end_seq,
    col = "#aaaaaa", lwd = 1)
# mapped planets per day (including rings)
mapped <- jett_plot(
    csv_file = "/home/poinck/gits/ed/jumps/mapped.csv",
    offset = 3125, factor = 5,
    column = "mapped", "sum",
    label = "surface scans / day",
    dt_seq = start_end_seq,
    col = "yellow", lwd = 1)
# bio per day
bio <- jett_plot(
    csv_file = "/home/poinck/gits/ed/jumps/bio.csv",
    offset = 5300, factor = 5,
    column = "scanned", "sum",
    label = "bios / day",
    dt_seq = start_end_seq,
    col = "cyan", lwd = 1)

# list plants
# - show most found plant (7)
plants <- aggregate(bio$scanned, by = list(plant = bio$species), FUN = sum, na.rm = TRUE)
plants <- plants[order(plants$x, decreasing = TRUE), ]
str(plants)
for (i in 7:1) {
    if (i > 4) {
        alpha <- 14 - i
        col <- paste0("#008b8b", alpha, alpha)
    } else {
        col <- paste0("cyan", i)
    }
    text(cx - radius, cy - radius + 5300 - (i * 250),
        col = col, font = 1,
        labels = paste0(plants$plant[i], " = ", plants$x[i]),
        cex = .7, pos = 4,
    )
}

# minimap
# - translate coordinates
# - no z-index: instead, use Count in "event":"FSSAllBodiesFound"
# - map above event to StarPos from "event":"FSDJump" by SystemAddress
#   which is unique
text(cx - radius - 40, cy - radius + 15170,
    col = "#eeeeee", font = 2,
    labels = "EXPLORATION",
    cex = 1, pos = 4,
)
text(cx, cy - radius + 15000,
    col = "#99999999", font = 1,
    labels = "https://gitlab.com/poinck/ed",
    cex = .7, pos = 3,
)
mxoff <- 1600
    # (3000 - 100) / 2 + 100
myoff <- 12750
rect(cx - radius + 100, cy - radius + 11800,
    cx - radius + 3100, cy - radius + 15000,
    border = "#eeeeee", lwd = .7, lty = 3, col = "#232323bb")
points(cx - radius + explored_bodies$x * .025 + mxoff,
    cy - radius + explored_bodies$y * .025 + myoff,
    pch = 3, col = "#aaaaaa12",
    cex = explored_bodies$count * .0123)

# TODO: change color based on planet class
# - different colors for terraformables (pink?) regardless of planet class
#cat(bold("explored_detailed_bodies:\n"))
detailed <- read.csv(
    paste0("/home/poinck/gits/ed/jumps/detailed.csv"),
)
#str(detailed)
explored_detailed_bodies <- merge(explored_bodies, detailed, by = "address")
cat("detailed length =", length(detailed$address), "\n")
#str(explored_detailed_bodies)

# list scanned planets
cat(bold("planets:\n"))
planets <- aggregate(rep(1, length(detailed$class)), by = list(class = detailed$class), FUN = sum, na.rm = TRUE)
planets <- planets[order(planets$x, decreasing = TRUE), ]
planets <- planets[planets$class == "Water world" | planets$class == "Ammonia world" | planets$class == "Earthlike body", ]
str(planets)
for (i in 3:1) {
    col <- paste0("yellow", i)
    text(cx - radius, cy - radius + 3100 - (i * 250),
        col = col, font = 1,
        labels = paste0(planets$class[i], " = ", planets$x[i]),
        cex = .7, pos = 4,
    )
}

# terraformables
# - blue (green-ish)
terra_bodies <- explored_detailed_bodies[explored_detailed_bodies$terraformable == "Terraformable", ]
terra_bodies_count <- length(terra_bodies$terraformable)
col <- paste0("yellow", 4)
text(cx - radius, cy - radius + 3100 - (4 * 250),
    col = col, font = 1,
    labels = paste0("Terraformable = ", terra_bodies_count),
    cex = .7, pos = 4,
)
points(cx - radius + terra_bodies$x * .025 + mxoff,
    cy - radius + terra_bodies$y * .025 + myoff,
    pch = 3, col = "#0078aa07",
    cex = terra_bodies$count * .0123)

# water bodies
# - red
water_bodies <- explored_detailed_bodies[explored_detailed_bodies$class == "Water world", ]
cat(bold("water_bodies:\n"))
str(water_bodies)
points(cx - radius + water_bodies$x * .025 + mxoff,
    cy - radius + water_bodies$y * .025 + myoff,
    pch = 3, col = "#aa000012",
    cex = water_bodies$count * .0123)

# earthlike bodies
# - green (yellow-ish)
earth_bodies <- explored_detailed_bodies[explored_detailed_bodies$class == "Earthlike body", ]
cat(bold("earth likes:\n"))
str(earth_bodies)
points(cx - radius + earth_bodies$x * .025 + mxoff,
    cy - radius + earth_bodies$y * .025 + myoff,
    pch = 3, col = "#78aa0023",
    cex = earth_bodies$count * .0246)

# label positions on minimap
for (i in 1:length(labels$x)) {
    points(cx - radius + labels$x[i] * .025 + mxoff,
        cy - radius + labels$y[i] * .025 + myoff,
        pch = c(rep(2, 4), 3),
        #col = "green4",
        col = c(rep("#bbbbbb", 4), "yellow"),
        lwd = c(rep(1.4, 4), 2.8),
        cex = c(rep(.4, 4), 3.2))
}

# current position in minimap
points(cx - radius + explored_bodies$x[length(explored_bodies$x)] * .025 + mxoff,
    cy - radius + explored_bodies$y[length(explored_bodies$y)] * .025 + myoff,
    pch = 1, col = "white", lwd = 1.6,
    cex = .6)

# boxplot (custom coords translated): z-index of EL, WW and TF

# analysis
terraformable_bodies <- explored_detailed_bodies[
    explored_detailed_bodies$terraformable != "", ]
terra_bodies$c <- 1

terra_sum_by_atmos <- with(terra_bodies, tapply(c, atmos, sum))
terra_sum_by_atmos <- sort(terra_sum_by_atmos)
#barplot(sort(terra_sum_by_atmos), horiz = TRUE, las = 1, col = "#69b3a2",
#    main = "Terraformables by atmosphere type")
#text(x = seq(0.65, 15.6, 1.2),
#    labels = paste0("      ", array(terra_sum_by_atmos)), pos = 4, font = 2)

terra_sum_by_class <- with(terra_bodies, tapply(c, class, sum))
terra_sum_by_class <- sort(terra_sum_by_class)
sum_by_class <- c(terra_sum_by_class, planets$x)
names(sum_by_class) <- c(dimnames(terra_sum_by_class)[[1]], planets$class)
sum_by_class[4] <- sum_by_class[4] - sum_by_class[2]
names(sum_by_class)[4] <- "Water world (not terraformable)"
names(sum_by_class)[5] <- "Ammonia world (not terraformable)"
names(sum_by_class)[6] <- "Earthlike body (not terraformable?)"
#barplot(sort(sum_by_class), horiz = TRUE, las = 1, col = "pink3",
#    main = "Terraformable planets")
#text(x = seq(0.69, 15.6, 1.2),
#    labels = paste0("      ", array(sort(sum_by_class))), pos = 4, font = 2)

biosignals <- read.csv(
    paste0("/home/poinck/gits/ed/jumps/signals.csv"),
)
biosignals$uid <- paste0(biosignals$address, biosignals$bodyid)
detailed$uid <- paste0(detailed$address, detailed$bodyid)
detailed_biosignals <- merge(biosignals, detailed, by = "uid",
    all.x = FALSE, all.y = FALSE, sort = FALSE)
detailed_biosignals <- detailed_biosignals[!duplicated(detailed_biosignals$uid), ]
detailed_biosignals <- detailed_biosignals[detailed_biosignals$uid != "NANA", ]
cat(bold("detailed_biosignals:\n"))
str(detailed_biosignals)
detailed_biosignals$c <- 1
bio_sum_by_atmos <- with(detailed_biosignals, tapply(c, atmos, sum))
bio_sum_by_atmos <- sort(bio_sum_by_atmos)
#barplot(sort(bio_sum_by_atmos), horiz = TRUE, las = 1, col = "yellow3",
#    main = "Bio signals (at least 1) by atmosphere type")
# text(x = seq(0.65, 18, 1.2),
#    labels = paste0("      ", array(bio_sum_by_atmos)), pos = 4, font = 2)

dev.off()
