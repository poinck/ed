#!/usr/bin/Rscript

require(rjson)
require(crayon)

#today <- strftime(Sys.time(), format = "%Y-%m-%d")
eddbdir <- "/home/poinck/gits/ed/eddb/"

factions_list <- fromJSON(file = paste0(eddbdir,
    "factions.json"))

# unique minor player faction IDs
mpfs <- vector()
for (fi in 1:length(factions_list)) {
    if (factions_list[[fi]]$is_player_faction == TRUE) {
        mpfs <- c(mpfs, factions_list[[fi]]$id)
    }
}
mpfs <- unique(mpfs)
str(mpfs)

stations_list <- fromJSON(file = paste0(eddbdir,
    "stations.json"))

systems_offlimit <- vector()
for (sti in 1:length(stations_list)) {
    cmf_id <- stations_list[[sti]]$controlling_minor_faction_id
    cat(" ", cmf_id)
    if (! is.null(cmf_id)) {
        if (stations_list[[sti]]$controlling_minor_faction_id %in% mpfs) {
            systems_offlimit <- c(systems_offlimit, stations_list[[sti]]$system_id)
            cat("-\n")
        }
    }
}
cat("\n")
systems_offlimit <- unique(systems_offlimit)
str(systems_offlimit)
    # FIXME: offlimiting by station is not enough. Ody-settlements and
    # installations seem to be missing here. Look at minor_faction_id in
    # systems_populated.json.

systems_list <- fromJSON(file = paste0(eddbdir,
    "systems_populated.json"))

systems_ok <- vector()
for (syi in 1:length(systems_list)) {
    system_id <- systems_list[[syi]]$id
    cat(" ", system_id)
    if (! is.null(system_id)) {
        if (! systems_list[[syi]]$id %in% systems_offlimit) {
            systems_ok <- c(systems_ok, systems_list[[syi]]$id)
            cat("+\n")
        }
    }
}
cat("\n")
systems_ok <- unique(systems_ok)
str(systems_ok)

npcsystems <- data.frame(
    system_id = vector(),
    system_name = vector(),
    power = vector(),
    power_id = vector(),
    x = vector(),
    y = vector(),
    z = vector(),
    allegiance_id = vector(),
    allegiance = vector(),
    government_id = vector(),
    government = vector(),
    primary_economy_id = vector(),
    primary_economy = vector(),
    population = vector(),
    mf_count = vector(),
    stations = vector(),
    starports = vector(),
    outposts = vector(),
    settlements = vector(),
    megashipfs = vector(),
    planetary_ports = vector(),
    S = vector(),
    M = vector(),
    L = vector(),
    material_traders = vector(),
    technology_brokers = vector(),
    interstellar_factors = vector()
)

for (syi in 1:length(systems_list)) {
    system_id <- systems_list[[syi]]$id
    if (! is.null(system_id)) {
        if (systems_list[[syi]]$id %in% systems_ok) {
            if (is.null(systems_list[[syi]]$power)) {
                power_name <- "None"
                power_id <- 0 
            } else {
                power_name <- systems_list[[syi]]$power
                if (is.null(systems_list[[syi]]$power_id)) {
                    power_id <- 0
                } else {
                    power_id <- systems_list[[syi]]$power_id
                }
            }
            if (is.null(systems_list[[syi]]$primary_economy)) {
                pe_name <- "None"
                pe_id <- 0 
            } else {
                pe_name <- systems_list[[syi]]$primary_economy
                if (is.null(systems_list[[syi]]$primary_economy_id)) {
                    pe_id <- 0
                } else {
                    pe_id <- systems_list[[syi]]$primary_economy_id
                }
            }
            system <- data.frame(
                system_id = system_id,
                system_name = systems_list[[syi]]$name,
                power = power_name,
                power_id = power_id,
                x = systems_list[[syi]]$x,
                y = systems_list[[syi]]$y,
                z = systems_list[[syi]]$z,
                allegiance_id = systems_list[[syi]]$allegiance_id,
                allegiance = systems_list[[syi]]$allegiance,
                government_id = systems_list[[syi]]$government_id,
                government = systems_list[[syi]]$government,
                primary_economy_id = pe_id,
                primary_economy = pe_name,
                population = systems_list[[syi]]$population,
                mf_count = length(systems_list[[syi]]$minor_faction_presences),
                native_pf_count = NA,
                stations = NA,
                starports = NA,
                outposts = NA,
                settlements = NA,
                megaships = NA,
                planetary_ports = NA,
                S = NA,
                M = NA,
                L = NA,
                material_traders = NA,
                technology_brokers = NA,
                interstellar_factors = NA
            )
            cat(".")
            npcsystems <- rbind(npcsystems, system)
        }
    }
}
str(npcsystems)
cat("\n")
saveRDS(npcsystems, paste0(eddbdir, "npcsystems.rds"))
write.csv(npcsystems,
    file = paste0(eddbdir, "npcsystems.csv")
)
cat(blue("saved systems as CSV.\n"))
