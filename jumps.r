#!/usr/bin/Rscript

require(crayon)

today <- strftime(Sys.time(), format = "%Y-%m-%d")
flat <- "Flat_Galaxy_Society"

jumps <- read.csv(
    paste0("/home/poinck/gits/ed/jumps/", today, "jumps1.csv"),
    #fill = TRUE, na.strings = "",
    #colClasses = c(
    #    "POSIXct", "factor", "factor", "factor", "numeric", "factor"
    #)
)
jumps$datetime <- as.POSIXct(jumps$datetime, tz = "UTC")
jumps$influence <- as.numeric(jumps$influence)
jumps$factionstate <- as.character(jumps$factionstate)
str(jumps)

flat_systems <- subset(jumps, faction == flat)
flats <- unique(flat_systems$system)
print(flats)
#arnais <- subset(flat_systems, system == "Arnais")
#psi <- subset(flat_systems, system == "14_Psi_Cancri")

influence_range <- range(c(0, 100
    #min(flat_systems$influence, na.rm = TRUE) * 100,
    #max(flat_systems$influence, na.rm = TRUE) * 100
))
time_range <- range(c(
    #max(flat_systems$datetime, na.rm = TRUE) - 5184000, # last 60 days
    max(flat_systems$datetime, na.rm = TRUE) - 2592000, # last 30 days
    max(flat_systems$datetime, na.rm = TRUE) + 259200   # future 3 days
))
png(filename = paste0("./jumps/", today, "jumps%03d.png"),
    family = "Monoid",
    #bg = "#cccccc",
    type = "cairo",
    #width = 768, height = 768,
    width = 820, height = 1117,
    pointsize = 14)
par(mar = c(5, 5, 2, 1),
    #xaxs = "i",
    yaxs = "i"
    #col = "white", col.axis = "white", col.lab = "white", col.main = "white",
    #col.sub = "white"
)

get_color <- function(flats, i, line = FALSE) {
    l_flats <- length(flats)

    c <- "#cccccc"
    if (line) {
        c <- hsv(i / l_flats, 1, .9, 1)
    } else {
        c <- hsv(i / l_flats, 1, .9, .5)
    }

    return(c)
}

get_colors <- function(state) {
    c <- vector()

    for (s in state) {
        c <- c(c, "lightgrey")
        if (! is.na(s)) {
            if (s == "War") {
                c <- c(c, "red2")
            } else if (s == "Expansion") {
                c <- c(c, "green3")
            } else if (s == "Boom") {
                c <- c(c, "darkgreen")
            } else if (s == "Blight") {
                c <- c(c, "purple")
            } else if (s == "None") {
                c <- c(c, "darkorange")
            }
        }
    }

    return(c)
}

i <- 1
fgs_cols <- vector()
for (f in flats) {
    if (i > 1) {
        par(new = TRUE)
    }
    fgs <- subset(flat_systems, system == f)
    print(fgs$factionstate)
    #fgs_cols <- c(fgs_cols, get_color(flats, i, TRUE))
    plot(fgs$datetime, fgs$influence * 100,
        type = "l", ylim = influence_range, xlim = time_range,
        xlab = "", ylab = "",
        lwd = 2,
        #col = get_color(flats, i, TRUE),
        col = "white",
        axes = FALSE
    )
    par(new = TRUE)
    plot(fgs$datetime, fgs$influence * 100,
        type = "l", ylim = influence_range, xlim = time_range,
        xlab = "", ylab = "",
        lwd = 1, lty = 3,
        col = get_color(flats, i, TRUE),
        #col = get_colors(fgs$factionstate),
        axes = FALSE
    )
    par(new = TRUE)
    if (i == 1) {
        plot(fgs$datetime, fgs$influence * 100,
            type = "p", ylim = influence_range, xlim = time_range,
            xlab = "Time", ylab = "Influence (%)", lwd = 3,
            #col = get_color(flats, i),
            col = get_colors(fgs$factionstate),
            main = "FLAT GALAXY SOCIETY"
        )
    } else {
        plot(fgs$datetime, fgs$influence * 100,
            type = "p", ylim = influence_range, xlim = time_range,
            xlab = "", ylab = "", lwd = 3,
            #col = get_color(flats, i),
            col = get_colors(fgs$factionstate),
            axes = FALSE
        )
    }
    text(fgs$datetime[length(fgs$datetime)] + 21600, 
        fgs$influence[length(fgs$influence)] * 100 - .125,
        labels = gsub("_", " ", fgs$system[length(fgs$system)]),
        cex = .7, pos = 4,
        #col = get_colors(fgs$factionstate[length(fgs$factionstate)])
    )

    i <- i + 1
}
#print(fgs_cols)
flats <- gsub("_", " ", flats)
legend("topleft", legend = c(
        "None", "Expansion", "Boom", "War", "Blight", "(other)"),
    ncol = 7,
    col = c(
        "darkorange", "green3", "darkgreen", "red2", "purple", "lightgrey"), 
    pch = 1,
    pt.lwd = 3, cex = .8)
cat(bold(green("almost there")), "\n")

# old
#plot(arnais$datetime, arnais$influence * 100,
#    type = "p", ylim = influence_range, xlim = time_range,
#    xlab = "Time", ylab = "Influence (%)", lwd = 3, col = "darkorange",
#    main = "FLAT GALAXY SOCIETY"
#)
#par(new = TRUE)
#plot(arnais$datetime, arnais$influence * 100,
#    type = "l", ylim = influence_range, xlim = time_range,
#    xlab = "", ylab = "",
#    lwd = .75, col = "darkorange", lty = 3,
#    axes = FALSE
#)
#par(new = TRUE)
#plot(psi$datetime, psi$influence * 100,
#    type = "p", ylim = influence_range, xlim = time_range,
#    xlab = "", ylab = "", lwd = 3, col = "purple",
#    axes = FALSE
#)
#par(new = TRUE)
#plot(psi$datetime, psi$influence * 100,
#    type = "l", ylim = influence_range, xlim = time_range,
#    xlab = "", ylab = "",
#    lwd = .75, col = "purple", lty = 3,
#    axes = FALSE
#)
#legend("topleft", legend = c("Arnais", "14 Psi Cancri"),
#    col = c("darkorange", "purple"), pch = 1,
#    pt.lwd = 3, lty = 3, cex = .8)

dev.off()

warnings()
