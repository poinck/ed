#!/usr/bin/Rscript

require(rjson)
require(crayon)

#today <- strftime(Sys.time(), format = "%Y-%m-%d")
eddbdir <- "/home/poinck/gits/ed/eddb/"

systems_list <- fromJSON(file = paste0(eddbdir,
    "systems_populated.json"))
#print(str(systems_list[[2]]))

factions_list <- fromJSON(file = paste0(eddbdir,
    "factions.json"))
#print(names(factions_list))

systems <- data.frame(
    system_id = vector(),
    system_name = vector(),
    power = vector(),
    #power_id = vector(),
    faction_id = vector(),
    faction_name = vector(),
    x = vector(),
    y = vector(),
    z = vector(),
    allegiance_id = vector(),
    allegiance = vector(),
    government_id = vector(),
    government = vector(),
    primary_economy_id = vector(),
    primary_economy = vector(),
    population = vector(),
    mf_count = vector()
)

npcsystems <- systems

if (!file.exists(paste0(eddbdir, "systems.rds"))) {
    for (fi in 1:length(factions_list)) {
        if (factions_list[[fi]]$is_player_faction == TRUE) {
            #cat(factions_list[[fi]]$name, "\n")
            pf_id <- factions_list[[fi]]$id
            pf_name <- factions_list[[fi]]$name
            for (si in 1:length(systems_list)) {
                if (! is.null(systems_list[[si]]$controlling_minor_faction_id)) {
                if (pf_id == systems_list[[si]]$controlling_minor_faction_id) {
                    cat(systems_list[[si]]$controlling_minor_faction_id, 
                        pf_name, "\n")
                    cat(".")
                    s1 <- data.frame(
                        system_id = systems_list[[si]]$id,
                        system_name = systems_list[[si]]$name,
                        faction_id = pf_id,
                        faction_name = pf_name,
                        x = systems_list[[si]]$x,
                        y = systems_list[[si]]$y,
                        z = systems_list[[si]]$z,
                        allegiance_id = systems_list[[si]]$allegiance_id,
                        allegiance = systems_list[[si]]$allegiance,
                        government_id = systems_list[[si]]$government_id,
                        government = systems_list[[si]]$government,
                        primary_economy_id = systems_list[[si]]$primary_economy_id,
                        primary_economy = systems_list[[si]]$primary_economy,
                        population = systems_list[[si]]$population,
                        mf_count = length(systems_list[[si]]$minor_faction_presences)
                    )
                    systems <- rbind(systems, s1)
                    cat(si, "")
                }
                }
            } 
        }
    }
    cat("\n")
    saveRDS(systems, paste0(eddbdir, "systems.rds"))
} else {
    systems <- readRDS(paste0(eddbdir, "systems.rds"))
    cat(bold(blue("using")), "existing", bold("systems\n"))
}
print(str(systems))

# unique minor player faction IDs
mpfs <- vector()
for (fi in 1:length(factions_list)) {
    if (factions_list[[fi]]$is_player_faction == TRUE) {
        mpfs <- c(mpfs, factions_list[[fi]]$id)
    }
}
mpfs <- unique(mpfs)

# player factions: list to data.frame
pfs <- data.frame(
    pf_id = vector(),
    pf_name = vector(),
    updated_at = vector(),
    home_system_id = vector()
)
if (!file.exists(paste0(eddbdir, "factions.rds"))) {
    for (fi in 1:length(factions_list)) {
        cat(fi, "\n")
        if (! is.null(factions_list[[fi]]$home_system_id)) {
            pf <- data.frame(
                pf_id = factions_list[[fi]]$id,
                pf_name = factions_list[[fi]]$name,
                updated_at = factions_list[[fi]]$updated_at,
                home_system_id = factions_list[[fi]]$home_system_id
            )
            cat(".")
            pfs <- rbind(pfs, pf)
        }
    }
    saveRDS(pfs, paste0(eddbdir, "factions.rds"))
} else {
    pfs <- readRDS(paste0(eddbdir, "factions.rds"))
    cat(bold(blue("using")), "existing", bold("factions\n"))
}

if (!file.exists(paste0(eddbdir, "npcsystems.rds"))) {
    for (si in 1:length(systems_list)) {
        if (! is.null(systems_list[[si]]$controlling_minor_faction_id)) {
        if (! systems_list[[si]]$controlling_minor_faction_id %in% systems$pf_id) {
            #cat(systems_list[[si]]$controlling_minor_faction_id, 
            #    pf_name, "\n")
            if (is.null(systems_list[[si]]$primary_economy_id)) {
                spe_id <- 99
                spe_name <- "unknown"
            } else {
                spe_id <- systems_list[[si]]$primary_economy_id
                spe_name <- systems_list[[si]]$primary_economy
            }
            if (systems_list[[si]]$needs_permit == FALSE) {
                add_npcsystem <- TRUE
                cat(" ", si)
                if (length(systems_list[[si]]$minor_faction_presences) > 0) {
                    for (mfi in 1:length(systems_list[[si]]$minor_faction_presences)) {
                        if (systems_list[[si]]$minor_faction_presences[[mfi]]$minor_faction_id == 76101) {
                            cat(bold(red("Black Widow?\n")))
                        }
                        if (systems_list[[si]]$minor_faction_presences[[mfi]]$minor_faction_id %in% mpfs) {
                            add_npcsystem <- FALSE
                            if (systems_list[[si]]$minor_faction_presences[[mfi]]$minor_faction_id == 76101) {
                                cat(bold(red("do not add system with Black Widow!"), "in", systems_list[[si]]$name, "\n"))
                            }
                        }
                    }
                }
                if (add_npcsystem == TRUE) {
                    cat(".")
                    if (is.null(systems_list[[si]]$power)) {
                        power_name <- "None"
                        power_id <- 0 
                    } else {
                        power_name <- systems_list[[si]]$power
                        power_id <- systems_list[[si]]$power_id 
                    }
                    cat(".")
                    s1 <- data.frame(
                        system_id = systems_list[[si]]$id,
                        system_name = systems_list[[si]]$name,
                        power = power_name,
                        #power_id = power_id, 
                        faction_id = systems_list[[si]]$controlling_minor_faction_id,
                        faction_name = systems_list[[si]]$controlling_minor_faction,
                        x = systems_list[[si]]$x,
                        y = systems_list[[si]]$y,
                        z = systems_list[[si]]$z,
                        allegiance_id = systems_list[[si]]$allegiance_id,
                        allegiance = systems_list[[si]]$allegiance,
                        government_id = systems_list[[si]]$government_id,
                        government = systems_list[[si]]$government,
                        primary_economy_id = spe_id,
                        primary_economy = spe_name,
                        population = systems_list[[si]]$population,
                        mf_count = length(systems_list[[si]]$minor_faction_presences)
                    )
                    cat(".\n")
                    npcsystems <- rbind(npcsystems, s1)
                    #cat(si, "")
                }
            }
        }
        }
    }
    cat("\n")
    saveRDS(npcsystems, paste0(eddbdir, "npcsystems.rds"))
} else {
    npcsystems <- readRDS(paste0(eddbdir, "npcsystems.rds"))
    cat(bold(blue("using")), "existing", bold("npcsystems\n"))
}

stations <- data.frame(
    system_id = vector(),
    station_name = vector(),
    station_type = vector(),
    max_landing_pad_size = vector(),
    planetary = vector(),
    has_material_trader = vector(),
    has_technology_broker = vector(),
    has_interstellar_factors = vector()
)

# add station information for npcsystems
if (! file.exists(paste0(eddbdir, "stations.rds"))) {
    stations_list <- fromJSON(file = paste0(eddbdir,
        "stations.json"))
    for (sti in 1:length(stations_list)) {
        cat(" ", sti)
        if (stations_list[[sti]]$system_id %in% npcsystems$system_id) {
        if (! stations_list[[sti]]$controlling_minor_faction_id %in% mpfs) {
            st1 <- data.frame(
                system_id = stations_list[[sti]]$system_id,
                station_name = stations_list[[sti]]$name,
                station_type = stations_list[[sti]]$type,
                max_landing_pad_size = stations_list[[sti]]$max_landing_pad_size,
                planetary = stations_list[[sti]]$is_planetary,
                has_material_trader = stations_list[[sti]]$has_material_trader,
                has_technology_broker = stations_list[[sti]]$has_technology_broker,
                has_interstellar_factors = stations_list[[sti]]$has_interstellar_factors
            )
            cat(".\n")
            stations <- rbind(stations, st1)
        }
        }
    }
    
    cat("\n")
    saveRDS(stations, paste0(eddbdir, "stations.rds"))
} else {
    stations <- readRDS(paste0(eddbdir, "stations.rds"))
    cat(bold(blue("using")), "existing", bold("stations\n"))
}

cat(bold("npc stations:\n"))
str(stations)

# caluclate number of stations for each system:
# - total
# - by type, eg. settlement, starport, outpost, asteroid base, planetary port, megaship 
# - by max landing pad size: S, M, L
# - number of planetary stations (not needed, see type)
# - number of stations with material trader
# - number of stations with technology broker
# - number of stations with interstellar factor
npcsystems$native_pf_count <- NA
npcsystems$stations <- NA
npcsystems$starports <- NA
npcsystems$outposts <- NA
npcsystems$settlements <- NA
npcsystems$megaships <- NA
npcsystems$planetary_ports <- NA
npcsystems$S <- NA
npcsystems$M <- NA
npcsystems$L <- NA
npcsystems$material_traders <- NA
npcsystems$technology_brokers <- NA
npcsystems$interstellar_factors <- NA
npcsystems$rejected <- NA
for (npci in 1:length(npcsystems$system_id)) {
    cat(" ", npci)
    native_pf_count <- length(pfs$home_system_id[pfs$home_system_id == npcsystems$system_id[npci]])
    station_count_total <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]]])
    station_count_type_starport <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & grepl("Starport", stations$station_type, fixed = TRUE)])
    station_count_type_outpost <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & grepl("Outpost", stations$station_type, fixed = TRUE)])
    station_count_type_settlement <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & grepl("Settlement", stations$station_type, fixed = TRUE)])
    station_count_type_megaship <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & grepl("Megaship", stations$station_type, fixed = TRUE)])
    station_count_type_planetary_port <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & grepl("Planetary Port", stations$station_type, fixed = TRUE)])
    station_count_S <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & stations$max_landing_pad_size == "S"])
    station_count_M <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & stations$max_landing_pad_size == "M"])
    station_count_L <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & stations$max_landing_pad_size == "L"])
    station_count_mt <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & stations$has_material_trader == TRUE])
    station_count_tb <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & stations$has_technology_broker == TRUE])
    station_count_if <- length(stations$system_id[stations$system_id == npcsystems$system_id[[npci]] & stations$has_interstellar_factors == TRUE])
    cat(".")
    npcsystems$native_pf_count[[npci]] <- native_pf_count
    npcsystems$stations[[npci]] <- station_count_total
    npcsystems$starports[[npci]] <- station_count_type_starport
    npcsystems$outposts[[npci]] <- station_count_type_outpost
    npcsystems$settlements[[npci]] <- station_count_type_settlement
    npcsystems$megaships[[npci]] <- station_count_type_megaship
    npcsystems$planetary_ports[[npci]] <- station_count_type_planetary_port
    npcsystems$S[[npci]] <- station_count_S
    npcsystems$M[[npci]] <- station_count_M
    npcsystems$L[[npci]] <- station_count_L
    npcsystems$material_traders[[npci]] <- station_count_mt
    npcsystems$technology_brokers[[npci]] <- station_count_tb
    npcsystems$interstellar_factors[[npci]] <- station_count_if
    cat(".\n")
}

str(npcsystems)

# calculate distances from SOL
cat(green("pre-filtered: no permit, no PMF present, less than 7 factions\n"))
npcsystems$dsol <- sqrt(npcsystems$x**2 + npcsystems$y**2 + npcsystems$z**2)
str(npcsystems)

cat(yellow("filter: within 500 Ly from SOL\n"))
npcsystems <- npcsystems[npcsystems$dsol < 500, ]
str(npcsystems)

#cat(yellow("filter: allegiance = Independent\n"))
#npcsystems <- npcsystems[npcsystems$allegiance_id == 4, ]
#str(npcsystems)

cat(yellow("filter: with less than 7 factions\n"))
npcsystems <- npcsystems[npcsystems$mf_count < 7, ]
str(npcsystems)

cat(yellow("filter: with more than 0 population\n"))
npcsystems <- npcsystems[npcsystems$population > 0, ]
str(npcsystems)

cat(blue("save system names as CSV\n"))
write.csv(npcsystems,
    file = paste0(eddbdir, "filtered_npcsystems.csv"),
)


# update manually edited old npc_systems
old_npcsystems <- read.csv(paste0(eddbdir, "filtered_npcsystems_old.csv"))
#old_npcsystems$rejected <- NA
    # XXX: remove line above before next re-imprt
str(old_npcsystems)
for (i_ons in 1:length(old_npcsystems$system_name)) {
    system_name <- old_npcsystems$system_name[i_ons]
    if (length(npcsystems$power[npcsystems$system_name == system_name]) > 0) {
        cat("update:", system_name, "\n")
        old_npcsystems$power[i_ons] <- npcsystems$power[npcsystems$system_name == system_name] 
        old_npcsystems$faction_name[i_ons] <- npcsystems$faction_name[npcsystems$system_name == system_name] 
        old_npcsystems$allegiance[i_ons] <- npcsystems$allegiance[npcsystems$system_name == system_name] 
        old_npcsystems$government[i_ons] <- npcsystems$government[npcsystems$system_name == system_name] 
        old_npcsystems$primary_economy[i_ons] <- npcsystems$primary_economy[npcsystems$system_name == system_name] 
        old_npcsystems$population[i_ons] <- npcsystems$population[npcsystems$system_name == system_name] 
        old_npcsystems$mf_count[i_ons] <- npcsystems$mf_count[npcsystems$system_name == system_name] 
        # assumption: amount of stations (starports, outposts, .., traders,
        # factors and brokers) does not change
        old_npcsystems$megaships[i_ons] <- npcsystems$megaships[npcsystems$system_name == system_name]
        old_npcsystems$native_pf_count[i_ons] <- npcsystems$native_pf_count[npcsystems$system_name == system_name]
    }
}

# add new npc systems previously not in the list
for (i_nsys in 1:length(npcsystems)) {
    if (! npcsystems$system_name[i_nsys] %in% old_npcsystems$system_name) {
        cat("add:", npcsystems$system_name[i_nsys], "\n")
        add_system <- data.frame(
            X = npcsystems$system_id[i_nsys],
            system_name = npcsystems$system_name[i_nsys],
            power = npcsystems$power[i_nsys],
            #faction_id = npcsystems$faction_id[i_nsys],
            faction_name = npcsystems$faction_name[i_nsys],
            x = npcsystems$x[i_nsys],
            y = npcsystems$y[i_nsys],
            z = npcsystems$z[i_nsys],
            #allegiance_id = npcsystems$allegiance_id[i_nsys],
            allegiance = npcsystems$allegiance[i_nsys],
            #government_id = npcsystems$government_id[i_nsys],
            government = npcsystems$government[i_nsys],
            #primary_economy_id = npcsystems$primary_economy_id[i_nsys],
            primary_economy = npcsystems$primary_economy[i_nsys],
            population = npcsystems$population[i_nsys],
            mf_count = npcsystems$mf_count[i_nsys],
            dsol = npcsystems$dsol[i_nsys],
            native_pf_count = npcsystems$native_pf_count[i_nsys],
            stations = npcsystems$stations[i_nsys],
            starports = npcsystems$starports[i_nsys],
            outposts = npcsystems$outposts[i_nsys],
            settlements = npcsystems$settlements[i_nsys],
            megaships = npcsystems$megaships[i_nsys],
            planetary_ports = npcsystems$planetary_ports[i_nsys],
            S = npcsystems$S[i_nsys],
            M = npcsystems$M[i_nsys],
            L = npcsystems$L[i_nsys],
            material_traders = npcsystems$material_traders[i_nsys],
            technology_brokers = npcsystems$technology_brokers[i_nsys],
            interstellar_factors = npcsystems$interstellar_factors[i_nsys],
            our.priority = "",
            bgs.situation = "",
            min.PMF.dist = "",
            pp.situation = "",
            vista = "",
            has.RES = "",
            closest.starport = "",
            has.4.natives = "",
            no.communists = "",
            max.traffic = "",
            has.carrier = "",
            anarchy.mf.count = "",
            rejected = ""
        )
        #cat(".")
        old_npcsystems <- rbind(old_npcsystems, add_system)
    }
}
merged_npcsystems <- old_npcsystems

# remove npcsystems not in new npcsystems list
for (i_msys in 1:length(merged_npcsystems)) {
    if (! merged_npcsystems$system_name[i_msys] %in% npcsystems$system_name) {
        cat("remove: ", merged_npcsystems$system_name[i_msys], "\n")
        merged_npcsystems <- merged_npcsystems[-c(i_msys), ]   
    }
}
cat(bold("merged"), "npcsystems:\n")
str(merged_npcsystems)

write.csv(merged_npcsystems,
    file = paste0(eddbdir, "filtered_npcsystems_merged.csv")
)
cat(blue("saved merged systems as CSV.\n"))

systems$g_col <- NA
systems$g_col[systems$government_id == 16] <- "#aaaaaa54"
    # Anarchy
systems$g_col[systems$government_id == 64] <- rgb(139/255, 10/255, 80/255, .34)
    # Corporate: deeppink4
systems$g_col[systems$government_id == 144] <- rgb(238/255, 0, 0, .34)
    # Patronage: red2
systems$g_col[systems$government_id == 112] <- rgb(238/255, 59/255, 59/255, .34)
    # Dictatorship: brown2
systems$g_col[systems$government_id == 96] <- rgb(0, 0, 205/255, .34)
    # Democracy: blue3
systems$g_col[systems$government_id == 80] <- rgb(0, 139/255, 139/255, .34)
    # Cooperative: cyan4
systems$g_col[systems$government_id == 160] <- rgb(139/255, 139/255, 0, .34)
    # Theocracy: yellow4
systems$g_col[systems$government_id == 48] <- rgb(0, 205/255, 0, .34)
    # Confederacy: green3
systems$g_col[systems$government_id == 128] <- rgb(0, 100/255, 0, .34)
    # Feudal: darkgreen 
systems$g_col[systems$government_id == 32] <- rgb(1, 140/255, 0, .34)
    # Communism: darkorange
systems$g_col[systems$government_id == 176] <- rgb(1, 1, 1, .34)
    # None: white
systems$g_col[systems$government_id == 150] <- rgb(0, 0, 0, .34)
    # Prison Colony: black

x_range <- range(c(-275, 225)) 
y_range <- range(c(-250, 250)) 

png(filename = paste0(eddbdir, "bubble.png"),
    family = "56th Street",
    bg = "#eeeeee",# col = "#eeeeee",
    type = "cairo",
    #width = 900, height = 900,
    width = 1800, height = 1800,
    pointsize = 14
)
par(mar = c(3, 3, 4, 1), mfrow = c(2, 2))

# governments
plot(systems$y, systems$x,
    col = systems$g_col,
    ylim = y_range, xlim = x_range, xlab = "", ylab = "",
    main = "B U B B L E\ngovernments",
    pch = 19, lwd = 3,
    cex = systems$population * .00000000025
)
legend("topright",
    legend = unique(systems$government), col = unique(systems$g_col),
    pch = 19, lwd = 3, lty = NA
)

systems$e_col <- NA
systems$e_col[systems$primary_economy_id == 7] <- "#aaaaaa54"
    # Service: gray
systems$e_col[systems$primary_economy_id == 9] <- rgb(139/255, 10/255, 80/255, .34)
    # Tourism: deeppink4
systems$e_col[systems$primary_economy_id == 4] <- rgb(238/255, 0, 0, .34)
    # Industrial: red2
systems$e_col[systems$primary_economy_id == 2] <- rgb(238/255, 59/255, 59/255, .34)
    # Extraction: brown2
systems$e_col[systems$primary_economy_id == 5] <- rgb(0, 0, 205/255, .34)
    # Military: blue3
systems$e_col[systems$primary_economy_id == 3] <- rgb(0, 139/255, 139/255, .34)
    # High Tech: cyan4
systems$e_col[systems$primary_economy_id == 8] <- rgb(139/255, 139/255, 0, .34)
    # Terraforming : yellow4
systems$e_col[systems$primary_economy_id == 11] <- rgb(0, 205/255, 0, .34)
    # Colony: green3
systems$e_col[systems$primary_economy_id == 1] <- rgb(0, 100/255, 0, .34)
    # Agriculture: darkgreen 
systems$e_col[systems$primary_economy_id == 6] <- rgb(1, 140/255, 0, .34)
    # Refinery: darkorange
systems$e_col[systems$primary_economy_id == 10] <- rgb(1, 1, 1, .34)
    # None: white

# economies:
plot(systems$y, systems$x,
    col = systems$e_col,
    ylim = y_range, xlim = x_range, xlab = "", ylab = "",
    main = "B U B B L E\nprimary economies",
    pch = 19, lwd = 3,
    cex = systems$population * .00000000025
)
legend("topright",
    legend = unique(systems$primary_economy), col = unique(systems$e_col),
    pch = 19, lwd = 3, lty = NA
)

systems$a_col <- NA
systems$a_col[systems$allegiance_id == 5] <- "#aaaaaa54"
    # None: gray
systems$a_col[systems$allegiance_id == 4] <- rgb(1, 140/255, 0, .34)
    # Independent: darkorange
systems$a_col[systems$allegiance_id == 3] <- rgb(0, 0, 205/255, .34)
    # Federation: blue3
systems$a_col[systems$allegiance_id == 2] <- rgb(238/255, 0, 0, .34)
    # Empire: red2
systems$a_col[systems$allegiance_id == 1] <- rgb(0, 205/255, 0, .34)
    # Alliance: green3
    
# economies:
plot(systems$y, systems$x,
    col = systems$a_col,
    ylim = y_range, xlim = x_range, xlab = "", ylab = "",
    main = "B U B B L E\nallegiancies",
    pch = 19, lwd = 3,
    cex = systems$population * .00000000025
)
legend("topright",
    legend = unique(systems$allegiance), col = unique(systems$a_col),
    pch = 19, lwd = 3, lty = NA
)    
    
# npc systems:
plot(npcsystems$y, npcsystems$x,
    col = rgb(0, 0, 0, .34),
    ylim = y_range, xlim = x_range, xlab = "", ylab = "",
    main = "B U B B L E\nsystems without player faction control",
    pch = 19, lwd = 3,
    cex = systems$population * .00000000025
)
legend("topright",
    legend = "npc systems", col = "black",
    pch = 19, lwd = 3, lty = NA
)    
cat(bold("governments:\n"))
print(unique(systems$government))
print(unique(systems$government_id))
cat(bold("economies:\n"))
print(unique(systems$primary_economy))
print(unique(systems$primary_economy_id))
cat(bold("allegiancies:\n"))
print(unique(systems$allegiance))
print(unique(systems$allegiance_id))

dev.off()

# bubble slices
zs <- data.frame(
    z1 = seq(-205, 205, by = 10),
    z2 = seq(-195, 215, by = 10)
)

require(raster)
e <- extent(c(-275, 225, -250, 250))
br <- raster(e, res = 10)


srs <- br
nrs <- br
drs <- br
for (zi in 1:length(zs$z1)) {
    # controlled population
    z_systems <- systems[systems$z > zs$z1[zi] & systems$z < zs$z2[zi], ]
    system_coords <- z_systems[c("x", "y")]
    scs <- data.frame(
        x = system_coords$y,
        y = system_coords$x
    )
    if (length(scs$x) > 0) {
        sbr <- rasterize(scs, field = systems$population,
            background = 0, fun = sum, br)
        srs <- stack(srs, sbr)
    } else {
        nabr <- br
        values(nabr) <- 0
        srs <- stack(srs, nabr)
    }
    
    # uncontrolled npc population
    z_npcsystems <- npcsystems[npcsystems$z > zs$z1[zi] & npcsystems$z < zs$z2[zi], ]
    npcsystem_coords <- z_npcsystems[c("x", "y")]
    ncs <- data.frame(
        x = npcsystem_coords$y,
        y = npcsystem_coords$x
    )
    if (length(ncs$x) > 0) {
        nbr <- rasterize(ncs, field = npcsystems$population,
            background = 0, fun = sum, br)
        nrs <- stack(nrs, nbr)
    } else {
        nabr <- br
        values(nabr) <- 0
        nrs <- stack(nrs, nabr)
    }
    
    # difference between controlled and uncontrolled population
    # - in Millions
    dbr <- srs[[zi]] - nrs[[zi]]
    dbr <- dbr / 1000000
    drs <- stack(drs, dbr)
    
    #cat("zi:", zi, "\n")
}

cat(bold("plots:\n"))
for (zi in 1:42) {
    #cat("zi:", zi, "\n")
    png(filename = paste0(eddbdir, "bubble", zi, ".png"),
        family = "56th Street",
        bg = "#cccccc",# col = "#eeeeee",
        type = "cairo",
        width = 900, height = 900,
        pointsize = 14
    )
    par(mar = c(3, 3, 5, 1))
    hgs <- colorRampPalette(c("seagreen3", "#dddddd", "#dddddd", "hotpink3"))
    pmins <- seq(cellStats(drs[[zi]], stat = "min", na.rm = TRUE),
        0, length.out = 6)
    pmaxs <- seq(1, cellStats(drs[[zi]], stat = "max", na.rm = TRUE),
        length.out = 6)
    hgs_breaks <- c(pmins, pmaxs)
    hgs_breaks <- unique(hgs_breaks)
    col_count <- length(hgs_breaks)
    #print(hgs_breaks)
    lab_breaks <- signif(hgs_breaks, digits = 3)
    lab_breaks <- unique(lab_breaks)
    #print(lab_breaks)
    plot(drs[[zi]],
        main = paste0("B U B B L E\n",
            "Controlled vs. uncontrolled population at z = ",
            zs$z1[zi], "..", zs$z2[zi], " (in Millions)"
        ),
        col = hgs(col_count), breaks = hgs_breaks, lab.breaks = lab_breaks
    )

    dev.off()
}

cat(bold(green("success?\n")))